# Webarchitects Redis Ansible Role

This repository contains an Ansible role for installing [Redis](https://redis.io/) on Debian and Ubuntu servers.

The only change from the default configuration is to add the following to `/etc/sysctl.conf`

```ini
vm.overcommit_memory = 1
```

And the following to `/etc/redis/redis.conf`:

```txt
unixsocket /run/redis/redis-server.sock
unixsocketperm 770
```

Which enables users who are in the `redis` group to use Redis via a UNIX socket in addition to TCP/IP to the `localhost` on port number `6379`.

## Role variables

See the [defaults/main.yml](defaults/main.yml) file for the default variables, the [vars/main.yml](vars/main.yml) file for the preset variables and the [meta/argument_specs.yml](meta/argument_specs.yml) file for the variable specification.

### redis

Set the `redis` variable to `true` for the tasks in this role to be run, it defaults to `false`.

### redis_config

A list of Redis config that should present, for example, the [defaults/main.yml](defaults/main.yml) have:

```yaml
redis_config:
  - name: unixsocket
    value: /run/redis/redis-server.sock
  - name: unixsocketperm
    value: "770"
```

If the `name` matches a value in the `redis_protected_configs` list (this list is incomplete), which not in the [vars/main.yml](vars/main.yml) file then the Ansible [lineinfile](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/lineinfile_module.html) module is used to update the setting, if not then the [community.general.redis](https://docs.ansible.com/ansible/latest/collections/community/general/redis_module.html) module is used.

### redis_verify

Check variables that start with `redis_` using the [meta/argument_specs.yml](meta/argument_specs.yml), this is a stricter check than Ansible uses by default as defined variables, not in the arg spec, such as `redis_foo` cause the verification to fail.

## Copyright

Copyright 2018-2024 Chris Croome, &lt;[chris@webarchitects.co.uk](mailto:chris@webarchitects.co.uk)&gt;.

This role is released under [the same terms as Ansible itself](https://github.com/ansible/ansible/blob/devel/COPYING), the [GNU GPLv3](LICENSE).
